package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class ThreadPoolConfig {

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(1);  // Minimum number of threads in the pool
        executor.setMaxPoolSize(5);  // Maximum number of threads in the pool
        executor.setQueueCapacity(1500);  // Number of tasks that can be queued if the pool is full. If there are 250 conccurent users firing request every second, then there should at least be 250 queue capacity
        executor.setThreadNamePrefix("MyThread-");  // Prefix for thread names
        executor.initialize();
        return executor;
    }

}
