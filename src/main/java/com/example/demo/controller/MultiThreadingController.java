package com.example.demo.controller;

import com.example.demo.service.MultiThreadingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MultiThreadingController {

    private final MultiThreadingService multiThreadingService;

    public MultiThreadingController(MultiThreadingService multiThreadingService) {
        this.multiThreadingService = multiThreadingService;
    }

    @GetMapping("/trigger")
    public void processUserDataAsync() {
        multiThreadingService.processUserDataAsync();
    }

    @GetMapping("/generateFileAsync")
    public void generateFileAsync() {
        multiThreadingService.generateFileAsync();
    }

    @GetMapping("/generateFileSync")
    public void generateTextSync() {
        multiThreadingService.generateFileSync();
    }

    @GetMapping("/generateFileAsyncForkPool")
    public void generateTextAsyncForkPool() {
        multiThreadingService.generateTextAsyncWithCustomForkPool();
    }
}
