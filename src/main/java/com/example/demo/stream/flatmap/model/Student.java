package com.example.demo.stream.flatmap.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Student {

    String name;
    List<String> dreamJobs;

    Integer age;

    public Student(String name, List<String> dreamJobs, Integer age) {
        this.name = name;
        this.dreamJobs = dreamJobs;
        this.age = age;
    }
}
