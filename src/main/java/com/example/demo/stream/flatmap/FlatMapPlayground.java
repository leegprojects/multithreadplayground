package com.example.demo.stream.flatmap;

import com.example.demo.commonUtils.CommonUtils;
import com.example.demo.stream.flatmap.model.Student;
import com.example.demo.stream.flatmap.utils.Generator;
import org.apache.logging.log4j.util.Strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FlatMapPlayground {

    /**
     * From a list of students, get a list of distinct jobs with flatmap
     */
    public void getListOfDistinctDreamJobs() {
        List<Student> students = new ArrayList<>();
        for (int i = 0; i < CommonUtils.random.nextInt(3,10); i++) {
            students.add(Generator.getStudent());
        }

        List<String> distinctDreamJobs = students.stream().map(student -> student.getDreamJobs()).flatMap(dreamJobs -> dreamJobs.stream()).distinct().collect(Collectors.toList());
        System.out.println(distinctDreamJobs);
    }

    /**
     * Challenging Task: Exploring Nested Lists
     * Suppose you have a list of lists of integers,
     * and you want to create a new list that contains the squares of all distinct even numbers present in the original lists.
     */
    public void challengingTask1() {
        List<List<Integer>> nestedLists = Arrays.asList(
                Arrays.asList(1, 2, 3, 4),
                Arrays.asList(5, 6, 7, 8),
                Arrays.asList(9, 10, 11, 12)
        );

        List<Integer> distinctSquaredEvenNumbers = nestedLists.stream().flatMap(list -> list.stream()).filter(num -> num%2 == 0).distinct().map(evenNum -> evenNum*evenNum).toList();
        System.out.println(distinctSquaredEvenNumbers);
    }

    /**
     * Challenging Task: Exploring Nested Lists
     * Suppose you have a list of students,
     * and you want to create a new list that contains distinct dreamjobs only for students that are over 25 years old
     */
    public void challengingTask2() {
        List<Student> students = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            students.add(Generator.getStudent());
        }

        List<String> distinctDreamJobsOfStudentsOver25 =
                students.stream()
                        .filter(student -> student.getAge() > 25)
                        .flatMap(studentOver25 -> studentOver25.getDreamJobs().stream())
                        .distinct().toList();
        System.out.println(distinctDreamJobsOfStudentsOver25);
    }
}
