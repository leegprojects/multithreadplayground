package com.example.demo.stream.flatmap.utils;

import com.example.demo.commonUtils.CommonUtils;
import com.example.demo.stream.flatmap.model.Student;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {

    public static Student getStudent() {
        List<String> dreamJobs = new ArrayList<>();
        for (int i = 0; i < CommonUtils.random.nextInt(5); i++) {
            dreamJobs.add(CommonUtils.faker.job().title());
        }
        return new Student(CommonUtils.faker.name().name(), dreamJobs, CommonUtils.random.nextInt(18, 69));
    }
}
