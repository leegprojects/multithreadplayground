package com.example.demo;

import com.example.demo.service.MultiThreadingService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.CompletableFuture;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);

//		MultiThreadingService service = context.getBean(MultiThreadingService.class);
//		CompletableFuture<Void> future = service.processUserDataAsync("userId", "usrename", "email");
//
//		System.out.println("Action carried out by main? - " + " (Thread: " + Thread.currentThread().getName() + ")");
//
//		// Wait for the asynchronous task to complete (if needed)
////		future.join();

	}

}
