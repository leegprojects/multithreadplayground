package com.example.demo.service;

import com.example.demo.dao.TestVariable;
import com.example.demo.dao.TestVariableDAO;
import jakarta.transaction.Transactional;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;

@Service
public class AsyncService {

    private final TestVariableDAO testVariableDAO;

    private final Random random = new Random();

    public AsyncService(TestVariableDAO testVariableDAO) {
        this.testVariableDAO = testVariableDAO;
    }

    @Async
    public CompletableFuture<Void> executeGetSaveDelete() {
        CompletableFuture<Optional<TestVariable>> futureGet = get();
        CompletableFuture<TestVariable> futureSave = save(new TestVariable());
        CompletableFuture<Void> futureDelete = delete();

        return CompletableFuture.completedFuture(null);
    }

    @Async
    public CompletableFuture<Optional<TestVariable>> get() {
        System.out.println("get is executed by " + Thread.currentThread().getName());
        return CompletableFuture.completedFuture(testVariableDAO.findById(1L));
    }

    @Async
    public CompletableFuture<TestVariable> save(TestVariable tv) {
        tv.setId(random.nextLong());
        System.out.println("save is executed by " + Thread.currentThread().getName());
        return CompletableFuture.completedFuture(testVariableDAO.save(tv));
    }

    @Async
    public CompletableFuture<Void> delete() {
        System.out.println("delete is executed by " + Thread.currentThread().getName());
        testVariableDAO.deleteById(1L);
        return CompletableFuture.completedFuture(null);
    }
}
