package com.example.demo.service;

import com.example.demo.dao.TestVariableDAO;
import jakarta.transaction.Transactional;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

@Service
public class MultiThreadingService {

    private final Random random = new Random();

    // ASCII printable characters range from 32 to 126
    private final int lowerBound = 32;
    private final int upperBound = 126;

    private final int textSize = 1024 * 1024;

    private final int chunks = 1000;

    private final TestVariableDAO testVariableDAO;

    private final AsyncService asyncService;

    private final TaskExecutor taskExecutor;

    public MultiThreadingService(TestVariableDAO testVariableDAO, AsyncService asyncService, TaskExecutor taskExecutor) {
        this.testVariableDAO = testVariableDAO;
        this.asyncService = asyncService;
        this.taskExecutor = taskExecutor;
    }

    @Transactional
    public CompletableFuture<Void> processUserDataAsync() {
        for (int i = 0; i < 5; i++) {
            asyncService.executeGetSaveDelete();
        }


//        return CompletableFuture.allOf(futureGet, futureSave, futureDelete);
        return CompletableFuture.completedFuture(null);
    }

    public void generateFileSync() {
        long startTime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < chunks; i++) {
            sb.append(generateTextSync());
        }

        try {
            printTimeTaken(startTime, "generateFileSync", "generate text");
            Path path = Path.of("generateFileSync.txt");

            // Write the content to the file
            Files.write(path, sb.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);

            printTimeTaken(startTime, "generateFileSync", "generate file");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void generateFileAsync() {
        long startTime = System.currentTimeMillis();

        CompletableFuture<String>[] futures = new CompletableFuture[chunks];
        for (int i = 0; i < chunks; i++) {
            futures[i] = generateTextAsync();
        }

        CompletableFuture<Void> allOf = CompletableFuture.allOf(futures);

        try {
            allOf.get(); // Blocking until all tasks are complete
            printTimeTaken(startTime, "generateFileAsync", "generate text");
            Path path = Path.of("generateFileAsync.txt");

            // Write the content to the file
            for (CompletableFuture<String> future : futures) {
                Files.write(path, future.get().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            }
            printTimeTaken(startTime, "generateFileAsync", "generate file");
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printTimeTaken(long startTime, String functionName, String additionalMessage) {
        System.out.println("Total time taken for " + functionName + " to finish " + additionalMessage + " is : " + (System.currentTimeMillis() - startTime) + " milliseconds");
    }

    public CompletableFuture<String> generateTextAsync() {
        return CompletableFuture.supplyAsync(() -> generateText("generateTextAsync"), taskExecutor);
    }

    public String generateTextSync() {
        return generateText("generateTextSync");
    }

    public String generateText(String functionName) {
        StringBuilder sb = new StringBuilder();
        System.out.println("Thread running in " + functionName + " is :" + Thread.currentThread().getName());
        while(sb.length() < textSize) {
            sb.append(lowerBound + random.nextInt(upperBound - lowerBound + 1));
        }

        return sb.toString();
    }

    public void generateTextAsyncWithCustomForkPool() {
        System.out.println("starting generateTextAsyncWithCustomForkPool");
        long startTime = System.currentTimeMillis();
        int parallelism = Runtime.getRuntime().availableProcessors();
        System.out.println("number of available processors is " + parallelism);
        // Create a CompletableFuture using the custom ForkJoinPool
        ForkJoinPool forkJoinPool = new ForkJoinPool(parallelism);

        CompletableFuture<String>[] futures = new CompletableFuture[chunks];
        for (int i = 0; i < chunks; i++) {
            futures[i] = CompletableFuture.supplyAsync(() -> generateText("generateTextAsyncWithCustomForkPool"), forkJoinPool);
        }

        CompletableFuture<Void> allOf = CompletableFuture.allOf(futures);

        try {
            allOf.get(); // Blocking until all tasks are complete
            printTimeTaken(startTime, "generateFileAsyncForkPool", "generate text");
            Path path = Path.of("generateFileAsyncForkPool.txt");

            // Write the content to the file
            for (CompletableFuture<String> future : futures) {
                Files.write(path, future.get().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            }
            printTimeTaken(startTime, "generateFileAsyncForkPool", "generate file");
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
